"""
Manager Module for defining shell commands for
    1. running application:
        - "python manage.py run"

    2. running tests:
        - "python manage.py test"

    3. database migration:
        - "python manage.py db init"  -> initialize database
        - "python manage.py db migrate - message 'testmessage'"  -> create migrations
        - "python manage.py db upgrade"  -> apply migrations to database
"""

import os

import pytest
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager

from app import init_api
# first import of main -> causes main __init__ to initialize SQLAlchemy object db
from app.main import create_app, db, api

##################################### CREATE APPLICATION #####################################

# create application and initialize api
app = create_app(os.getenv('GARTZ_INVEST_ENV') or 'dev')
init_api(api)
app.app_context().push()

#################################### ADD MANAGER COMMANDS ####################################

# initialize Manager, handling a set of commands to execute from shell
manager = Manager(app)

# integrate SQLAlchemy with application
migrate = Migrate(app, db)

# add db commands to manager, allowing for migration commands from shell
manager.add_command('db', MigrateCommand)


@manager.command
def run():
    """manager command to run application"""
    app.run(host='0.0.0.0')


@manager.command
def test():
    """manager command to run tests"""
    pytest.main(['-ra'])


if __name__ == '__main__':
    """starts manager with 'python manager.py' allowing to execute registered commands"""
    manager.run()
