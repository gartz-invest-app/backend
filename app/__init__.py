"""
Initialization of top level package "app"
executed at first import of the "app" package in manage.py file
"""

from app.main.account.account_controller import acc_bp
from app.main.auth.auth_controller import auth_bp
from app.main.contract.contract_controller import contract_bp
from app.main.tag.tag_controller import tag_bp
from app.main.tag_group.tag_group_controller import tag_grp_bp
from app.main.transaction.transaction_controller import tx_bp
from app.main.user.user_controller import user_bp

#################################### REGISTER API BLUEPRINTS ####################################

authorizations = {
    'type': 'apiKey',
    'in': 'header',
    'name': 'Authorization', }


def init_api(api):
    api.spec.components.security_scheme('ApiKeyAuth', authorizations)  # register apikey authentication
    api.register_blueprint(auth_bp)  # authentication operations
    api.register_blueprint(user_bp)  # user operations
    api.register_blueprint(acc_bp)  # account operations
    api.register_blueprint(contract_bp)  # contract operations
    api.register_blueprint(tx_bp)  # transaction operations
    api.register_blueprint(tag_bp)  # tag operations
    api.register_blueprint(tag_grp_bp)  # group operations
