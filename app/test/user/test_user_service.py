import pytest
from datetime import datetime
from app.main.user.user_service import UserService
from app.main.user.user_model import User


#REQUEST_PATH = '/users'
#def test_create_user(client, session):
#    re = client.post(REQUEST_PATH, json={'username': 'Max',
#                                         'email': 'max@email.de',
#                                         'password': '12345678',
#                                         'language': 'ENG',
#                                         'time_zone': 'Europe/Berlin'}, follow_redirects=True)
#    assert re.status_code == 201



def test_create_user(session, user_data_list):
    """test best case user create operation"""
    start = datetime.utcnow()
    for user in user_data_list:
        UserService.create(user_data=user)
    end = datetime.utcnow()

    users_created = User.query.all()
    assert len(users_created) == len(user_data_list)

    for c, (user, data) in enumerate(zip(users_created, user_data_list)):
        assert user.id > 0
        assert start <= user.last_update <= end
        assert start <= user.registered_on <= end
        assert not user.premium
        assert not user.admin
        for k, v in data.items():
            if k == 'password':
                assert user.check_password(v)
            else:
                assert getattr(user, k) == v


def test_create_user_duplicate(session, init_users, user_data):
    UserService.create(user_data=user_data)




@pytest.mark.parametrize('ids, expected',
                         [(None, 4),
                          ([], 0),
                          ([1, 2], 2)])
def test_read_all_users(session, init_users, ids, expected):
    assert len(UserService.read_all(ids=ids)) == expected


