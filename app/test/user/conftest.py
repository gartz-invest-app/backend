import pytest
from datetime import datetime
from app.main.user.user_model import User

user_data_1 = {'username': 'Max', 'email': 'max@email.de',
               'password': '12345678', 'language': 'ENG',
               'time_zone': 'Europe/Berlin'}

user_data_2 = {'username': 'Michael', 'email': 'michael@email.de',
               'password': '12345678', 'language': 'DE',
               'time_zone': 'Europe/Berlin'}

user_data_3 = {'username': 'Sina', 'email': 'sina@email.de',
               'password': '12345678', 'language': 'ENG',
               'time_zone': 'Europe/Berlin'}

user_data_4 = {'username': 'Kathrin', 'email': 'kathrin@email.de',
               'password': '12345678', 'language': 'DE',
               'time_zone': 'Europe/Berlin'}


@pytest.fixture(scope='module')
def user_data():
    return user_data_1


@pytest.fixture(scope='module')
def user_data_list():
    return [user_data_1, user_data_2]


@pytest.fixture
def init_users(session):
    users_data = [user_data_1, user_data_2, user_data_3, user_data_4]
    users = [User(**data) for data in users_data]
    for c, user in enumerate(users):
        user.id = c+1
        user.admin = False
        user.premium = False
        user.registered_on = datetime.utcnow()
        user.last_update = user.registered_on
        session.add(user)
    session.commit()
    return users



# @pytest.fixture(scope='module', params=['Max', 'Michael123',
#                                         'Az', 'Max_1992'])
# def username(request):
#     return request.param
#
#
# @pytest.fixture(scope='module',
#                 params=['m.gartz@email.de', 'M.gartz@email.custom.de',
#                         'test.de', 'kathrin@test', 'max'])
# def email(request):
#     return request.param
#
#
# @pytest.fixture(scope='module', params=['123', 'Abc123!§$%&#', 'abcdefg12345_'])
# def password(request):
#     return request.param
#
#
# @pytest.fixture(scope='module', params=['DE', 'ENG', 'IT'])
# def language(request):
#     return request.param
#
#
# @pytest.fixture(scope='module', params=['test', pytz.common_timezones[0]])
# def time_zone(request):
#     return request.param


# @pytest.fixture(scope='module')
# def user_data(username, email, password, language, time_zone):
#     return {'username': username,
#             'email': email,
#             'password': password,
#             'language': language,
#             'time_zone': time_zone}
