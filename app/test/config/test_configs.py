"""Module for testing app configuration"""
import pytest

from app.main import create_app
from app.main.config import DevelopmentConfig, TestingConfig, ProductionConfig


@pytest.mark.parametrize("config, expected",
                         [('dev', DevelopmentConfig),
                          ('test', TestingConfig),
                          ('prod', ProductionConfig)])
def test_app_config(config, expected):
    test_app = create_app(config)

    attributes = [(k, getattr(expected, k)) for k in expected.__dict__ if not k.startswith('__')] \
                 + [(k, getattr(expected, k)) for k in expected.__base__.__dict__ if not k.startswith('__')]

    for k, v in attributes:
        assert test_app.config.get(k) == v
