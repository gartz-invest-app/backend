import pytest
from flask_migrate import Migrate, upgrade

from app import init_api
from app.main import create_app, api, db as _db


@pytest.fixture(scope='session')
def app(request):
    """initialize test application"""
    # create flask app and initialize api
    flask_app = create_app('test')
    init_api(api)

    # Establish an application context before running the tests.
    ctx = flask_app.app_context()
    ctx.push()

    def teardown():
        ctx.pop()

    request.addfinalizer(teardown)
    return flask_app


@pytest.fixture(scope='session')
def client(app):
    """provide Werkzeug test client"""
    return app.test_client()


@pytest.fixture(scope='session')
def db(request, app):
    """initialize the test database."""
    Migrate(app, _db)
    upgrade()

    def teardown():
        _db.drop_all()
        _db.engine.execute('DROP TABLE alembic_migrations')

    request.addfinalizer(teardown)
    return _db


@pytest.fixture(scope='function')
def session(request, db):
    """create a new database session for each test."""
    conn = db.engine.connect()
    transaction = conn.begin()

    options = dict(bind=conn, binds={})
    session = db.create_scoped_session(options=options)

    db.session = session

    def teardown():
        transaction.rollback()
        conn.close()
        session.remove()

    request.addfinalizer(teardown)
    return session


@pytest.fixture(scope='class')
def class_session(request):
    """wrapper for class wide db session."""
    return session(request)
