from app.main import db
from app.main.account.account_model import Account
from app.main.tag.tag_model import Tag
from app.main.tag_group.tag_group_model import TagGroup
from app.main.transaction.transaction_model import Transaction
from app.main.utils.model_utils import transaction_tag_association_table
from app.main.utils.service_utils import GenericService


class TransactionService(GenericService):

    @classmethod
    def read_all(cls, ids=None, user_ids=None, account_ids=None, contract_ids=None, tag_ids=None, tag_group_ids=None):
        """
        Read transactions by filter.

        Parameters
        ----------
        ids : list of int
            ids of transactions to filter by
        user_ids : list of int
            user ids to filter transactions by
        account_ids : list of int
            account ids to filter transactions by
        contract_ids : list of int
            contract ids to filter transactions by
        tag_ids : list of int
            tag ids to filter transactions by
        tag_group_ids : list of int
            tag group ids to filter transactions by

        Returns
        -------
        list of :class:`Transaction`
            List of all transactions matching the filter.
        """
        if ids is None:
            ids_filter = True
        else:
            ids_filter = Transaction.id.in_(ids)

        if user_ids is None:
            user_ids_filter = True
        else:
            user_ids_filter = Transaction.user_id.in_(user_ids)

        if account_ids is None:
            account_ids_filter = True
        else:
            account_ids_filter = Transaction.account_id.in_(account_ids)

        if contract_ids is None:
            contract_ids_filter = True
        else:
            contract_ids_filter = Transaction.contract_id.in_(contract_ids)

        if tag_ids is None:
            tag_ids = []
            tag_ids_filter = True
        else:
            tag_ids_filter = transaction_tag_association_table.c.tag_id.in_(tag_ids)

        if tag_group_ids is not None:
            tag_groups = TagGroup.query.filter(TagGroup.id.in_(tag_group_ids)).all()
            tag_ids = list(set(id for id in tag_ids).union(tag.id for group in tag_groups for tag in group.tags))
            tag_ids_filter = transaction_tag_association_table.c.tag_id.in_(tag_ids)

        return Transaction.query.join(transaction_tag_association_table).join(Tag) \
            .filter(ids_filter, user_ids_filter, account_ids_filter, contract_ids_filter, tag_ids_filter).all()

    @classmethod
    def read_all_by_user(cls, user_id, **kwargs):
        """
        Read transactions of a user by filter.

        Parameters
        ----------
        user_id : int
            id of user to get transactions for
        **kwargs : optional
            keyword arguments for filtering

        Returns
        -------
        list of :class:`Transaction`
            List of transactions of the user matching the filter.
        """
        cls.validate_user_permission(user_id)

        return cls.read_all(user_ids=[user_id], **kwargs)

    @classmethod
    def create(cls, tx_data, user_id):
        """
        Create a new transaction for a user.

        Parameters
        ----------
        tx_data : dict
            deserialized request data for transaction (immutable)
        user_id : int
            id of user to create transaction for

        Returns
        -------
        :class:`Transaction`
            Created transaction.
        """
        cls.validate_user_permission(user_id)

        account = Account.query.filter(Account.id == tx_data['account_id'],
                                       Account.user_id == user_id).first()

        if not account:
            abort(404, message='There is no Account with the given identifier for this user.')

        data = dict(tx_data)
        data['tags'] = Tag.query.filter(Tag.id.in_(data.pop('tag_ids', None)),
                                        Tag.user_id == user_id).all()

        new_transaction = Transaction(**data)
        new_transaction.user_id = user_id

        account.last_transaction = new_transaction.date \
            if (not account.last_transaction) or (new_transaction.date > account.last_transaction) \
            else account.last_transaction

        db.session.add(new_transaction)
        db.session.add(account)
        db.session.commit()

        return new_transaction

    @classmethod
    def update(cls, tx_data, user_id, id):
        """
        Update a transaction of a user by id.

        Parameters
        ----------
        tx_data : dict
            deserialized request data for transaction (immutable)
        user_id : int
            id of user to update transaction for
        id : int
            id of transaction to update

        Returns
        -------
        :class:`Transaction`
            Updated transaction.
        """
        cls.validate_user_permission(user_id)

        transaction = Transaction.query.filter(Transaction.id == id,
                                               Transaction.user_id == user_id).first()

        if not transaction:
            abort(404, message='There is no Transaction with the given identifier for this user.')

        data = dict(tx_data)
        if 'tag_ids' in data.keys():
            data['tags'] = Tag.query.filter(Tag.id.in_(data.pop('tag_ids', None)),
                                            Tag.user_id == user_id).all()
        for k, v in data.items():
            setattr(transaction, k, v)

        new_account = Account.query.filter(Account.id == transaction.account_id,
                                           Account.user_id == user_id)
        if not new_account:
            abort(404, message='There is no Account with the given identifier for this user.')

        db.session.add(transaction)
        db.session.commit()

        return transaction

    @classmethod
    def read(cls, user_id, id):
        """
        Read a transaction of a user by id.

        Parameters
        ----------
        user_id : int
            id of user to get transaction for
        id : int
            id of transaction to get from database

        Returns
        -------
        :class:`Transaction`
            Retrieved transaction.
        """
        cls.validate_user_permission(user_id)

        transaction = Transaction.query.filter(Transaction.id == id,
                                               Transaction.user_id == user_id).first()

        if not transaction:
            abort(404, message='There is no Transaction with the given identifier for this user.')

        return transaction

    @classmethod
    def delete(cls, user_id, id):
        """
        Delete a transaction by id.

        Parameters
        ----------
        user_id : int
            id of user to delete transaction for
        id : int
            id of transaction to delete from database

        Returns
        -------
        :class:`Transaction`
            Deleted transaction.
        """
        cls.validate_user_permission(user_id)

        transaction = Transaction.query.filter(Transaction.id == id,
                                               Transaction.user_id == user_id).first()

        if not transaction:
            abort(404, message='There is no Transaction with the given identifier for this user.')

        db.session.delete(transaction)
        db.session.commit()

        return transaction
