"""Module for defining Transaction related models."""

import marshmallow as ma

from app.main import db
from app.main.tag.tag_model import TagSchema
from app.main.utils.model_utils import transaction_tag_association_table


class Transaction(db.Model):
    """Transaction Entity Model, defining transaction database table"""

    __tablename__ = 'transaction'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(250), nullable=False)

    date = db.Column(db.Date, nullable=False)
    tx_type = db.Column(db.String(10), nullable=False)

    currency = db.Column(db.String(10), nullable=False)
    amount = db.Column(db.Float(precision=2), nullable=False)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id', onupdate='CASCADE', ondelete='CASCADE'), nullable=False)
    user = db.relationship(
        'User',
        backref=db.backref('transactions', lazy=True)
    )

    account_id = db.Column(db.Integer, db.ForeignKey('account.id', onupdate='CASCADE', ondelete='RESTRICT'),
                           nullable=False)
    account = db.relationship(
        'Account',
        backref=db.backref('transactions', lazy=True)
    )

    contract_id = db.Column(db.Integer, db.ForeignKey('contract.id', onupdate='CASCADE', ondelete='SET NULL'),
                            nullable=True)
    contract = db.relationship(
        'Contract',
        backref=db.backref('transactions', lazy=True)
    )

    tags = db.relationship('Tag', secondary=transaction_tag_association_table, back_populates='transactions')

    note = db.Column(db.Text, nullable=False)

    def __repr__(self):
        return "<Transaction id: '{}', date: '{}', transaction-type: '{}', amount: '{}', " \
               "account: '{}', user: '{}'>".format(
            self.id, self.date, self.tx_type, self.amount, self.account.name, self.user.username)


class TransactionSchema(ma.Schema):
    """Transaction schema for serialization and deserialization"""

    class Meta:
        ordered = True

    id = ma.fields.Integer(dump_only=True)
    name = ma.fields.String(
        required=True,
        validate=ma.validate.Regexp(
            '[a-zA-Z0-9_.&%$-]{3,}',
            error='invalid contract name (only letters, 0-9 and _ . & % $ - are allowed, min 3 symbols)'
        )
    )

    date = ma.fields.Date(required=True)
    tx_type = ma.fields.String(
        required=True,
        validate=ma.validate.OneOf(
            choices=['Expense', 'Income'],
            error='invalid transaction type (only "Income" or "Expense")'
        ),
    )

    currency = ma.fields.String(
        required=True,
        validate=ma.validate.OneOf(
            choices=['EUR', 'USD'],
            error='invalid currency'
        ),
    )

    amount = ma.fields.Float(required=True)

    user_id = ma.fields.Integer(dump_only=True)
    account_id = ma.fields.Integer(required=True)
    contract_id = ma.fields.Integer(dump_only=True)

    tag_ids = ma.fields.List(ma.fields.Integer(), required=True, load_only=True)
    tags = ma.fields.List(ma.fields.Nested(TagSchema), dump_only=True)
    note = ma.fields.String(missing='')


class TransactionPartialSchema(TransactionSchema):
    """Transaction Update Schema Model for serialization and deserialization"""
    pass


class TransactionQueryParams(ma.Schema):
    """Transaction Schema for query parameters"""

    class Meta:
        ordered = True

    ids = ma.fields.List(ma.fields.Integer())
    user_ids = ma.fields.List(ma.fields.Integer())
    account_ids = ma.fields.List(ma.fields.Integer())
    contract_ids = ma.fields.List(ma.fields.Integer())
    tag_ids = ma.fields.List(ma.fields.Integer())
    tag_group_ids = ma.fields.List(ma.fields.Integer())
