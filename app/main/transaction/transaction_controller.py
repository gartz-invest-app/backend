""" Module for Routing of Transaction related requests. """

from flask.views import MethodView
from flask_jwt_extended import fresh_jwt_required
from flask_smorest import Blueprint

from app.main.transaction.transaction_model import TransactionSchema, TransactionPartialSchema, TransactionQueryParams
from app.main.transaction.transaction_service import TransactionService
from app.main.utils.controller_utils import ErrorDocs, AccessHandler

tx_bp = Blueprint('users/transactions', 'users/transaction', url_prefix='/users',
                  description='Transactions related operations')


@tx_bp.route('/transactions')
class TransactionsAdmin(MethodView):
    """
    Methods:
        - GET: get transactions of all users
    """

    @fresh_jwt_required
    @AccessHandler.required_access_level('Admin')
    @tx_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401]))
    @tx_bp.arguments(TransactionQueryParams, location='query', as_kwargs=True)
    @tx_bp.response(TransactionSchema(many=True), code=200, description='Successfully retrieved transactions.')
    def get(self, **kwargs):
        """List all transactions."""
        return TransactionService.read_all(**kwargs)


@tx_bp.route('/<int:user_id>/transactions', parameters=[
    {'name': 'user_id', 'in': 'path', 'description': 'user id'},
])
class TransactionsUser(MethodView):
    """
    Methods:
        - GET: get transactions of a user
    """

    @fresh_jwt_required
    @AccessHandler.required_access_level('User')
    @tx_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401, 404]))
    @tx_bp.arguments(TransactionQueryParams(exclude=['user_ids']), location='query', as_kwargs=True)
    @tx_bp.response(TransactionSchema(many=True), code=200, description='Successfully retrieved transactions.')
    def get(self, user_id, **kwargs):
        """List all transactions of a user."""
        return TransactionService.read_all_by_user(user_id=user_id, **kwargs)


@tx_bp.route('/<int:user_id>/transactions', parameters=[
    {'name': 'user_id', 'in': 'path', 'description': 'user id'},
])
class Transaction(MethodView):
    """
    Methods:
        - POST: create a new transaction for a user
    """

    @fresh_jwt_required
    @AccessHandler.required_access_level('User')
    @tx_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401, 404, 409, 422]))
    @tx_bp.arguments(TransactionSchema, location='json')
    @tx_bp.response(TransactionSchema, code=201, description='Successfully created transaction.')
    def post(self, tx_data, user_id):
        """Create a new transaction for a user."""
        return TransactionService.create(tx_data=tx_data, user_id=user_id)


@tx_bp.route('/<int:user_id>/transactions/<int:id>', parameters=[
    {'name': 'user_id', 'in': 'path', 'description': 'user id'},
    {'name': 'id', 'in': 'path', 'description': 'transaction id'},
])
class TransactionById(MethodView):
    """
    Methods:
        - GET: get a transaction of a user
        - PUT: update a transaction of a user
        - DELETE: delete a transaction of a user
    """

    @fresh_jwt_required
    @AccessHandler.required_access_level('User')
    @tx_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401, 404]))
    @tx_bp.response(TransactionSchema, code=200, description='Successfully retrieved transaction.')
    def get(self, user_id, id):
        """Get a transaction of a user given its id."""
        return TransactionService.read(user_id=user_id, id=id)

    @fresh_jwt_required
    @AccessHandler.required_access_level('User')
    @tx_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401, 404, 422]))
    @tx_bp.arguments(TransactionPartialSchema(partial=True), location='json')
    @tx_bp.response(TransactionSchema, code=200, description='Successfully updated transaction.')
    def put(self, tx_data, user_id, id):
        """Update a transaction of a user given its id."""
        return TransactionService.update(tx_data=tx_data, user_id=user_id, id=id)

    @fresh_jwt_required
    @AccessHandler.required_access_level('User')
    @tx_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401, 404]))
    @tx_bp.response(TransactionSchema, code=200, description='Successfully deleted transaction.')
    def delete(self, user_id, id):
        """Delete a transaction of a user given its id."""
        return TransactionService.delete(user_id=user_id, id=id)
