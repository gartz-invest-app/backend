"""
Module for defining tag group models.
A tag group captures a set of tags.
"""

import marshmallow as ma
from sqlalchemy import UniqueConstraint

from app.main import db
from app.main.tag.tag_model import TagSchema
from app.main.utils.model_utils import tag_group_association_table


class TagGroup(db.Model):
    """TagGroup Entity Model, defining group database table"""

    __tablename__ = 'tag_group'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255), nullable=False)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='CASCADE'), nullable=False)
    user = db.relationship(
        'User',
        backref=db.backref('tag_groups', lazy=True)
    )

    tags = db.relationship('Tag', secondary=tag_group_association_table, back_populates='tag_groups')
    note = db.Column(db.Text, nullable=False)

    __table_args__ = (
        UniqueConstraint('user_id', 'name', name='user_id_tag_group_name_uk'),
    )

    def __repr__(self):
        return "<TagGroup id: '{}', name: '{}', user: '{}'>".format(
            self.id, self.name, self.user.username)


class TagGroupSchema(ma.Schema):
    """TagGroup Schema Model for serialization and deserialization"""

    class Meta:
        ordered = True

    id = ma.fields.Integer(dump_only=True)
    name = ma.fields.String(
        required=True,
        validate=ma.validate.Regexp(
            '[a-zA-Z0-9_.&%$-]{3,}',
            error='invalid tag group name (only letters, 0-9 and _ . & % $ - are allowed, min 3 symbols)'
        ),
    )
    user_id = ma.fields.Integer(dump_only=True)
    tag_ids = ma.fields.List(ma.fields.Integer(), required=True, load_only=True)
    tags = ma.fields.List(ma.fields.Nested(TagSchema), dump_only=True)

    note = ma.fields.String(missing='')


class TagGroupPartialSchema(TagSchema):
    """TagGroup Update Schema Model for serialization and deserialization"""
    pass


class TagGroupQueryParams(ma.Schema):
    """TagGroup Schema for query parameters"""

    class Meta:
        ordered = True

    ids = ma.fields.List(ma.fields.Integer())
    user_ids = ma.fields.List(ma.fields.Integer())
    tag_ids = ma.fields.List(ma.fields.Integer())
