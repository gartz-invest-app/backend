""" Module for Routing of TagGroup related requests. """

from flask.views import MethodView
from flask_jwt_extended import fresh_jwt_required
from flask_smorest import Blueprint

from app.main.tag_group.tag_group_model import TagGroupSchema, TagGroupPartialSchema, TagGroupQueryParams
from app.main.tag_group.tag_group_service import TagGroupService
from app.main.utils.controller_utils import ErrorDocs, AccessHandler

tag_grp_bp = Blueprint('users/tag-groups', 'users/tag-groups', url_prefix='/users',
                       description='TagGroups related operations')


@tag_grp_bp.route('/tag-groups')
class TagGroupsAdmin(MethodView):
    """
    Methods:
        - GET: get tag groups of all users
    """

    @fresh_jwt_required
    @AccessHandler.required_access_level('Admin')
    @tag_grp_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401]))
    @tag_grp_bp.arguments(TagGroupQueryParams, location='query', as_kwargs=True)
    @tag_grp_bp.response(TagGroupSchema(many=True), code=200, description='Successfully retrieved tag groups.')
    def get(self, **kwargs):
        """List all tag groups."""
        return TagGroupService.read_all(**kwargs)


@tag_grp_bp.route('/<int:user_id>/tag-groups', parameters=[
    {'name': 'user_id', 'in': 'path', 'description': 'user id'},
])
class TagGroupsUser(MethodView):
    """
    Methods:
        - GET: get tag groups of a user
    """

    @fresh_jwt_required
    @AccessHandler.required_access_level('User')
    @tag_grp_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401]))
    @tag_grp_bp.arguments(TagGroupQueryParams(exclude=['user_ids']), location='query')
    @tag_grp_bp.response(TagGroupSchema(many=True), code=200, description='Successfully retrieved tag groups.')
    def get(self, user_id, **kwargs):
        """List all tag groups of a user."""
        return TagGroupService.read_all_by_user(user_id=user_id, **kwargs)


@tag_grp_bp.route('/<int:user_id>/tag-groups', parameters=[
    {'name': 'user_id', 'in': 'path', 'description': 'user id'},
])
class TagGroup(MethodView):
    """
    Methods:
        - POST: create a tag group for a user
    """

    @fresh_jwt_required
    @AccessHandler.required_access_level('User')
    @tag_grp_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401, 409, 422]))
    @tag_grp_bp.arguments(TagGroupSchema, location='json')
    @tag_grp_bp.response(TagGroupSchema, code=201, description='Successfully created tag group.')
    def post(self, tag_group_data, user_id):
        """Create a new tag group for a user."""
        return TagGroupService.create(tag_group_data=tag_group_data, user_id=user_id)


@tag_grp_bp.route('/<int:user_id>/tag-groups/<int:id>', parameters=[
    {'name': 'user_id', 'in': 'path', 'description': 'user id'},
    {'name': 'id', 'in': 'path', 'description': 'tag group id'},
])
class GroupById(MethodView):
    """
    Methods:
        - GET: get a tag group of a user
        - PUT: update a tag group of a user
        - DELETE: delete a tag group of a user
    """

    @fresh_jwt_required
    @AccessHandler.required_access_level('User')
    @tag_grp_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401, 404]))
    @tag_grp_bp.response(TagGroupSchema, code=200, description='Successfully retrieved tag group.')
    def get(self, user_id, id):
        """Get a tag group of a user given its id."""
        return TagGroupService.read(user_id=user_id, id=id)

    @fresh_jwt_required
    @AccessHandler.required_access_level('User')
    @tag_grp_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401, 404, 422]))
    @tag_grp_bp.arguments(TagGroupPartialSchema(partial=True), location='json')
    @tag_grp_bp.response(TagGroupSchema, code=200, description='Successfully updated tag group.')
    def put(self, tag_group_data, user_id, id):
        """Update a tag group of a user given its id."""
        return TagGroupService.update(tag_group_data=tag_group_data, user_id=user_id, id=id)

    @fresh_jwt_required
    @AccessHandler.required_access_level('User')
    @tag_grp_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401, 404]))
    @tag_grp_bp.response(TagGroupSchema, code=200, description='Successfully deleted tag group.')
    def delete(self, user_id, id):
        """Delete a tag group of a user given its id."""
        return TagGroupService.delete(user_id=user_id, id=id)
