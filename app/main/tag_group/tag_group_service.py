from flask_smorest import abort

from app.main import db
from app.main.tag.tag_model import Tag
from app.main.tag_group.tag_group_model import TagGroup
from app.main.utils.model_utils import tag_group_association_table
from app.main.utils.service_utils import GenericService


class TagGroupService(GenericService):

    @classmethod
    def read_all(cls, ids=None, user_ids=None, tag_ids=None):
        """
        Read tag groups by filter.

        Parameters
        ----------
        ids : list of int
            ids of tag groups to filter by
        user_ids : list of int
            user ids to filter tag groups by
        tag_ids : list of int
            tag ids to filter tag groups by

        Returns
        -------
        list of :class:`TagGroup`
            List of all tag groups matching the filter.
        """
        if ids is None:
            ids_filter = True
        else:
            ids_filter = TagGroup.id.in_(ids)

        if user_ids is None:
            user_ids_filter = True
        else:
            user_ids_filter = TagGroup.user_id.in_(user_ids)

        if tag_ids is None:
            tag_ids_filter = True
        else:
            tag_ids_filter = tag_group_association_table.c.tag_id.in_(tag_ids)

        return TagGroup.query.join(tag_group_association_table, isouter=True).join(Tag, isouter=True) \
            .filter(ids_filter, user_ids_filter, tag_ids_filter).all()

    @classmethod
    def read_all_by_user(cls, user_id, **kwargs):
        """
        Read all tag groups of a user by filter.

        Parameters
        ----------
        user_id : int
            id of user to get tag groups for
        **kwargs : optional
            keyword arguments for filtering

        Returns
        -------
        list of :class:`TagGroup`
            List of tag groups of the user matching the filter.
        """
        cls.validate_user_permission(user_id)

        return cls.read_all(user_ids=[user_id], **kwargs)

    @classmethod
    def create(cls, tag_group_data, user_id):
        """
        Create a new tag group for a user.

        Parameters
        ----------
        tag_group_data : dict
            deserialized request data for tag group (immutable)
        user_id : int
            id of user to create tag group for

        Returns
        -------
        :class:`TagGroup`
            Created tag group.
        """
        cls.validate_user_permission(user_id)

        tag_group = TagGroup.query.filter(TagGroup.user_id == user_id,
                                          TagGroup.name == tag_group_data['name']).first()

        if not tag_group:
            data = dict(tag_group_data)
            data['tags'] = Tag.query.filter(Tag.id.in_(data.pop('tag_ids', None)),
                                            Tag.user_id == user_id).all()

            new_tag_group = TagGroup(**data)
            new_tag_group.user_id = user_id

            db.session.add(new_tag_group)
            db.session.commit()

            return new_tag_group
        else:
            abort(409, msg='TagGroup already exists.')

    @classmethod
    def update(cls, tag_group_data, user_id, id):
        """
        Update a tag group of a user by id.

        Parameters
        ----------
        tag_group_data : dict
            deserialized request data for tag group (immutable)
        user_id : int
            id of user to update tag group for
        id : int
            id of tag group to update

        Returns
        -------
        :class:`TagGroup`
            Updated tag group.
        """
        cls.validate_user_permission(user_id)

        tag_group = TagGroup.query.filter(TagGroup.id == id,
                                          TagGroup.user_id == user_id).first()

        if not tag_group:
            abort(404, message='There is no TagGroup with the given identifier for this user.')

        data = dict(tag_group_data)
        data['tags'] = Tag.query.filter(Tag.id.in_(data.pop('tag_ids', None)),
                                        Tag.user_id == user_id).all()

        for k, v in data.items():
            setattr(tag_group, k, v)

        db.session.add(tag_group)
        db.session.commit()

        return tag_group

    @classmethod
    def read(cls, user_id, id):
        """
        Read a tag group of a user by id.

        Parameters
        ----------
        user_id : int
            id of user to get tag group for
        id : int
            id of tag group to get from database

        Returns
        -------
        :class:`TagGroup`
            Retrieved tag group.
        """
        cls.validate_user_permission(user_id)

        tag_group = TagGroup.query.filter(TagGroup.id == id,
                                          TagGroup.user_id == user_id).first()

        if not tag_group:
            abort(404, message='There is no TagGroup with the given identifier for this user.')

        return tag_group

    @classmethod
    def delete(cls, user_id, id):
        """
        Delete a tag group by id.

        Parameters
        ----------
        user_id : int
            id of user to delete tag group for
        id : int
            id of tag group to delete from database

        Returns
        -------
        :class:`TagGroup`
            Deleted tag group.
        """
        cls.validate_user_permission(user_id)

        tag_group = TagGroup.query.filter(TagGroup.id == id,
                                          TagGroup.user_id == user_id).first()

        if not tag_group:
            abort(404, message='There is no TagGroup with the given identifier for this user.')

        db.session.delete(tag_group)
        db.session.commit()

        return tag_group
