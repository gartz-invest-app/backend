"""Module for defining Contract related models."""

import marshmallow as ma
import pytz
from sqlalchemy import UniqueConstraint

from app.main import db
from app.main.tag.tag_model import TagSchema
from app.main.transaction.transaction_model import Transaction
from app.main.utils.model_utils import contract_tag_association_table


class Contract(db.Model):
    """Contract Entity Model, defining contract database table"""

    __tablename__ = 'contract'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(250), nullable=False)

    tx_type = db.Column(db.String(10), nullable=False)
    payment_method = db.Column(db.Integer, nullable=False)
    start_date = db.Column(db.Date, nullable=False)
    end_date = db.Column(db.Date, nullable=False)
    hour_of_payment = db.Column(db.Integer, nullable=False)
    next_payment_date = db.Column(db.DateTime, nullable=True)

    currency = db.Column(db.String(10), nullable=False)
    amount = db.Column(db.Float(precision=2), nullable=False)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id', onupdate='CASCADE', ondelete='CASCADE'), nullable=False)
    user = db.relationship(
        'User',
        backref=db.backref('contracts', lazy=True)
    )

    account_id = db.Column(db.Integer, db.ForeignKey('account.id', onupdate='CASCADE', ondelete='RESTRICT'),
                           nullable=False)
    account = db.relationship(
        'Account',
        backref=db.backref('contracts', lazy=True)
    )

    tags = db.relationship('Tag', secondary=contract_tag_association_table, back_populates='contracts')

    note = db.Column(db.Text, nullable=False)

    __table_args__ = (
        UniqueConstraint('user_id', 'name', 'tx_type', name='user_id_contract_name_transaction_type_uk'),
    )

    def __repr__(self):
        return "<Contract id: '{}', name: '{}', transaction-type: '{}', payment-method: '{}', amount: '{}', " \
               "account: '{}', user: '{}'>".format(
            self.id, self.name, self.tx_type, self.payment_method, self.amount,
            self.account.name, self.user.username)

    def instantiate_transaction(self):
        """
        instantiate a transaction for this contract
        """
        return Transaction(name=self.name,
                           date=pytz.utc.localize(self.next_payment_date).astimezone(
                               pytz.timezone(self.user.time_zone)).date(),
                           tx_type=self.tx_type, currency=self.currency, user_id=self.user_id,
                           amount=self.amount, user=self.user, account_id=self.account_id,
                           account=self.account, contract_id=self.id, contract=self,
                           note='automatically generated transaction for contract: {}'.format(self.name))


class ContractSchema(ma.Schema):
    """Contract Schema Model for serialization and deserialization"""

    class Meta:
        ordered = True

    id = ma.fields.Integer(dump_only=True)
    name = ma.fields.String(
        required=True,
        validate=ma.validate.Regexp(
            '[a-zA-Z0-9_.&%$-]{3,}',
            error='invalid contract name (only letters, 0-9 and _ . & % $ - are allowed, min. 3 symbols)'
        )
    )

    tx_type = ma.fields.String(
        required=True,
        validate=ma.validate.OneOf(
            choices=['Expense', 'Income'],
            error='invalid transaction type (only "Income" or "Expense")'
        ),
    )

    payment_method = ma.fields.Integer(
        required=True,
        validate=ma.validate.OneOf(
            choices=[1, 3, 6, 12],
            error='invalid payment method (only 1,3,6,12)'
        )
    )

    start_date = ma.fields.Date(required=True)
    end_date = ma.fields.Date(required=True)

    hour_of_payment = ma.fields.Integer(
        required=True,
        validate=ma.validate.OneOf(
            choices=list(range(0, 23)),
            error='invalid hour of payment (only 0-23)'
        )
    )

    next_payment_date = ma.fields.DateTime(dump_only=True)

    currency = ma.fields.String(
        required=True,
        validate=ma.validate.OneOf(
            choices=['EUR', 'USD'],
            error='invalid currency'
        ),
    )

    amount = ma.fields.Float(required=True)

    user_id = ma.fields.Integer(dump_only=True)
    account_id = ma.fields.Integer(required=True)

    tag_ids = ma.fields.List(ma.fields.Integer(), required=True, load_only=True)
    tags = ma.fields.List(ma.fields.Nested(TagSchema), dump_only=True)
    note = ma.fields.String(missing='')


class ContractPartialSchema(ContractSchema):
    """Contract Update Schema Model for serialization and deserialization"""
    pass


class ContractQueryParams(ma.Schema):
    """Contract Schema for query parameters"""

    class Meta:
        ordered = True

    ids = ma.fields.List(ma.fields.Integer())
    user_ids = ma.fields.List(ma.fields.Integer())
    account_ids = ma.fields.List(ma.fields.Integer())
    tag_ids = ma.fields.List(ma.fields.Integer())
    tag_group_ids = ma.fields.List(ma.fields.Integer())
