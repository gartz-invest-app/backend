"""Module for Routing of Contract related requests."""

from flask.views import MethodView
from flask_jwt_extended import fresh_jwt_required
from flask_smorest import Blueprint

from app.main.contract.contract_model import ContractSchema, ContractQueryParams, ContractPartialSchema
from app.main.contract.contract_service import ContractService
from app.main.utils.controller_utils import ErrorDocs, AccessHandler

contract_bp = Blueprint('users/contracts', 'users/contracts', url_prefix='/users',
                        description='Contracts related operations')


@contract_bp.route('/contracts')
class ContractsAdmin(MethodView):
    """
    Methods:
        - GET: get contracts of all users
    """

    @fresh_jwt_required
    @AccessHandler.required_access_level('Admin')
    @contract_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401]))
    @contract_bp.arguments(ContractQueryParams, location='query', as_kwargs=True)
    @contract_bp.response(ContractSchema(many=True), code=200, description='Successfully retrieved contracts.')
    def get(self, **kwargs):
        """List all contracts."""
        return ContractService.read_all(**kwargs)


@contract_bp.route('/<int:user_id>/contracts', parameters=[
    {'name': 'user_id', 'in': 'path', 'description': 'user id'},
])
class ContractsUser(MethodView):
    """
    Methods:
        - GET: get contracts of a user
    """

    @fresh_jwt_required
    @AccessHandler.required_access_level('User')
    @contract_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401, 404]))
    @contract_bp.arguments(ContractQueryParams(exclude=['user_ids']), location='query', as_kwargs=True)
    @contract_bp.response(ContractSchema(many=True), code=200, description='Successfully retrieved contracts.')
    def get(self, user_id, **kwargs):
        """List all contracts of a user."""
        return ContractService.read_all_by_user(user_id=user_id, **kwargs)


@contract_bp.route('/<int:user_id>/contracts', parameters=[
    {'name': 'user_id', 'in': 'path', 'description': 'user id'},
])
class Contract(MethodView):
    """
    Methods:
        - POST: create a new contract for a user
    """

    @fresh_jwt_required
    @AccessHandler.required_access_level('User')
    @contract_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401, 404, 409, 422]))
    @contract_bp.arguments(ContractSchema, location='json')
    @contract_bp.response(ContractSchema, code=201, description='Successfully created contract.')
    def post(self, contract_data, user_id):
        """Create a new contract for a user."""
        return ContractService.create(contract_data=contract_data, user_id=user_id)


@contract_bp.route('/<int:user_id>/contracts/<int:id>', parameters=[
    {'name': 'user_id', 'in': 'path', 'description': 'user id'},
    {'name': 'id', 'in': 'path', 'description': 'contract id'},
])
class ContractById(MethodView):
    """
    Methods:
        - GET: get a contract of a user
        - PUT: update a contract of a user
        - DELETE: delete a contract of a user
    """

    @fresh_jwt_required
    @AccessHandler.required_access_level('User')
    @contract_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401, 404]))
    @contract_bp.response(ContractSchema, code=200, description='Successfully retrieved contract.')
    def get(self, user_id, id):
        """Get a contract given its id."""
        return ContractService.read(user_id=user_id, id=id)

    @fresh_jwt_required
    @AccessHandler.required_access_level('User')
    @contract_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401, 404, 422]))
    @contract_bp.arguments(ContractPartialSchema(partial=True), location='json')
    @contract_bp.response(ContractSchema, code=200, description='Successfully updated contract.')
    def put(self, contract_data, user_id, id):
        """Update a contract given its id."""
        return ContractService.update(contract_data=contract_data, user_id=user_id, id=id)

    @fresh_jwt_required
    @AccessHandler.required_access_level('User')
    @contract_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401, 404]))
    @contract_bp.response(ContractSchema, code=200, description='Successfully deleted contract.')
    def delete(self, user_id, id):
        """Delete a contract given its id."""
        return ContractService.delete(user_id=user_id, id=id)
