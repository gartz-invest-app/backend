"""Module for defining Service for Contract controller."""

from datetime import datetime

import pytz
from flask_smorest import abort

from app.main import db
from app.main.account.account_model import Account
from app.main.contract.contract_model import Contract
from app.main.tag.tag_model import Tag
from app.main.tag_group.tag_group_model import TagGroup
from app.main.utils.model_utils import contract_tag_association_table
from app.main.utils.service_utils import GenericService


class ContractService(GenericService):

    @classmethod
    def read_all(cls, ids=None, user_ids=None, account_ids=None, tag_ids=None, tag_group_ids=None):
        """
        Read contracts by filter.

        Parameters
        ----------
        ids : list of int
            ids of contracts to filter by
        user_ids : list of int
            user ids to filter contracts by
        account_ids : list of int
            account ids to filter contracts by
        tag_ids : list of int
            tag ids to filter contracts by
        tag_group_ids : list of int
            tag group ids to filter contracts by

        Returns
        -------
        list of :class:`Contract`
            List of all contracts matching the filter.
        """
        if ids is None:
            ids_filter = True
        else:
            ids_filter = Contract.id.in_(ids)

        if user_ids is None:
            user_ids_filter = True
        else:
            user_ids_filter = Contract.user_id.in_(user_ids)

        if account_ids is None:
            account_ids_filter = True
        else:
            account_ids_filter = Contract.account_id.in_(account_ids)

        if tag_ids is None:
            tag_ids = []
            tag_ids_filter = True
        else:
            tag_ids_filter = contract_tag_association_table.c.tag_id.in_(tag_ids)

        if tag_group_ids is not None:
            tag_groups = TagGroup.query.filter(TagGroup.id.in_(tag_group_ids)).all()
            tag_ids = list(set(id for id in tag_ids).union(tag.id for group in tag_groups for tag in group.tags))
            tag_ids_filter = contract_tag_association_table.c.tag_id.in_(tag_ids)

        return Contract.query.join(contract_tag_association_table).join(Tag) \
            .filter(ids_filter, user_ids_filter, account_ids_filter, tag_ids_filter).all()

    @classmethod
    def read_all_by_user(cls, user_id, **kwargs):
        """
        Read contracts of a user by filter.

        Parameters
        ----------
        user_id : int
            id of user to get contracts for
        **kwargs : optional
            keyword arguments for filtering

        Returns
        -------
        list of :class:`Contract`
            List of all contracts of the user.
        """
        cls.validate_user_permission(user_id)

        return cls.read_all(user_ids=[user_id], **kwargs)

    @classmethod
    def create(cls, contract_data, user_id):
        """
        Create a new contract for a user.

        Parameters
        ----------
        contract_data : dict
            deserialized request data for contract (immutable)
        user_id : int
            id of user to create contract for

        Returns
        -------
        :class:`Contract`
            Created contract.
        """
        current_user, requested_user = cls.validate_user_permission(user_id)

        contract = Contract.query.filter(Contract.user_id == user_id,
                                         Contract.name == contract_data['name'],
                                         Contract.tx_type == contract_data['tx_type']).first()

        if contract:
            abort(409, message='contract with given name, and transaction type already exists for this user.')

        account = Account.query.filter(Account.id == contract_data['account_id'],
                                       Account.user_id == user_id).first()

        if not account:
            abort(404, message='There is no Account with the given identifier for this user.')

        data = dict(contract_data)
        data['tags'] = Tag.query.filter(Tag.id.in_(data.pop('tag_ids', None)),
                                        Tag.user_id == user_id).all()

        new_contract = Contract(**data)
        new_contract.user_id = user_id

        user_tz = pytz.timezone(requested_user.time_zone)
        next_payment = datetime(new_contract.start_date.year,
                                new_contract.start_date.month,
                                new_contract.start_date.day,
                                new_contract.hour_of_payment)
        new_contract.next_payment_date = user_tz.normalize(user_tz.localize(next_payment)).astimezone(pytz.utc)

        db.session.add(new_contract)
        db.session.commit()

        return new_contract

    @classmethod
    def update(cls, contract_data, user_id, id):
        """
        Update a contract of a user by id.

        Parameters
        ----------
        contract_data : dict
            deserialized request data for contract (immutable)
        user_id : int
            id of user to update contract for
        id : int
            id of contract to update

        Returns
        -------
        :class:`Contract`
            Updated contract.
        """
        cls.validate_user_permission(user_id)

        contract = Contract.query.filter(Contract.id == id,
                                         Contract.user_id == user).first()

        if not contract:
            abort(404, message='There is no Contract with the given identifier for this user.')

        data = dict(contract_data)
        if 'tag_ids' in data.keys():
            data['tags'] = Tag.query.filter(Tag.id.in_(data.pop('tag_ids', None)),
                                            Tag.user_id == user_id).all()

        for k, v in data.items():
            setattr(contract, k, v)

        new_account = Account.query.filter(Account.id == contract.account_id,
                                           Account.user_id == user_id)
        if not new_account:
            abort(404, message='There is no Account with the given identifier for this user.')

        db.session.add(contract)
        db.session.commit()

        return contract

    @classmethod
    def read(cls, user_id, id):
        """
        Read a contract of a user by id.

        Parameters
        ----------
        user_id : int
            id of user to get contract for
        id : int
            id of contract to get from database

        Returns
        -------
        :class:`Contract`
            Retrieved contract.
        """
        cls.validate_user_permission(user_id)

        contract = Contract.query.filter(Contract.id == id,
                                         Contract.user_id == user_id).first()

        if not contract:
            abort(404, message='There is no Contract with the given identifier for this user.')

        return contract

    @classmethod
    def delete(cls, user_id, id):
        """
        Delete a contract by id.

        Parameters
        ----------
        user_id : int
            id of user to delete contract for
        id : int
            id of contract to delete from database

        Returns
        -------
        :class:`Contract`
            Deleted contract.
        """
        cls.validate_user_permission(user_id)

        contract = Contract.query.filter(Contract.id == id,
                                         Contract.user_id == user_id).first()

        if not contract:
            abort(404, message='There is no Contract with the given identifier for this user.')

        db.session.delete(contract)
        db.session.commit()

        return contract
