"""
Initialization of subpackage main
initializes SQLAlchemy, Marschmallow and Bcrypt and defines function for creating app
"""

from flask import Flask
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager
from flask_smorest import Api
from flask_sqlalchemy import SQLAlchemy

from app.main.config import config_by_name, Config
from app.main.scheduler.factory import FlaskAPScheduler

######################################### INITIALIZATIONS #########################################

api = Api()
db = SQLAlchemy()
bcrypt = Bcrypt()
jwt = JWTManager()
scheduler = FlaskAPScheduler()


######################################### DEFINE CREATE APP #########################################

def create_app(config_name):
    """
    creates an instance of the app with given config (selected by name),
    initializing Api, SQLAlchemy, Marshmallow etc. with the app
    """
    app = Flask(Config.APP_NAME)
    app.config.from_object(config_by_name[config_name])

    api.init_app(app, spec_kwargs={'security': [{'ApiKeyAuth': []}]})
    db.init_app(app)
    bcrypt.init_app(app)
    jwt.init_app(app)
    scheduler.init_app(app)

    return app
