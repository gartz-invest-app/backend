""" Module for defining Service for User controller. """

from datetime import datetime

from flask_smorest import abort

from app.main import db
from app.main.user.user_model import User
from app.main.utils.service_utils import GenericService


class UserService(GenericService):

    @classmethod
    def read_all(cls, ids=None):
        """
        Read all users by filter.

        Parameters
        ----------
        ids : list of int
            list of user ids to filter by

        Returns
        -------
        list of :class:`User`
            List of users matching the filter.
        """
        if ids is None:
            ids_filter = True
        else:
            ids_filter = User.id.in_(ids)

        return User.query.filter(ids_filter).all()

    @classmethod
    def create(cls, user_data):
        """
        Create a new user.

        Parameters
        ----------
        user_data : dict
            deserialized request data for user (immutable)

        Returns
        -------
        :class:`User`
            Created user.
        """
        user = User.query.filter((User.username == user_data['username']) | (User.email == user_data['email'])).first()

        if not user:
            new_user = User(**user_data)
            new_user.registered_on = datetime.utcnow()
            new_user.last_update = new_user.registered_on

            db.session.add(new_user)
            db.session.commit()

            return new_user
        else:
            abort(409, msg='User already exists.')

    @classmethod
    def update(cls, user_data, id):
        """
        Update a user by id.

        Parameters
        ----------
        user_data : dict
            deserialized request data for user (immutable)
        id : int
            id of user to update

        Returns
        -------
        :class:`User`
            Updated user.
        """
        super().validate_user_permission(id)

        user = User.query.filter(User.id == id).first()

        if not user:
            abort(404, message='There is no User with the given identifier.')

        for k, v in user_data.items():
            setattr(user, k, v)
        user.last_update = datetime.utcnow()

        db.session.add(user)
        db.session.commit()

        return user

    @classmethod
    def read(cls, id):
        """
        Read a user by id.

        Parameters
        ----------
        id : int
            id of user to get from database

        Returns
        -------
        :class:`User`
            Retrieved user.
        """
        super().validate_user_permission(id)

        user = User.query.filter(User.id == id).first()

        if not user:
            abort(404, message='There is no User with the given identifier.')

        return user

    @classmethod
    def delete(cls, id):
        """
        Delete a user by id.

        Parameters
        ----------
        id : int
            id of user to delete from database

        Returns
        -------
        :class:`User`
            Deleted user.
        """
        super().validate_user_permission(id)

        user = User.query.filter(User.id == id).first()

        if not user:
            abort(404, message='There is no User with the given identifier.')

        db.session.delete(user)
        db.session.commit()

        return user
