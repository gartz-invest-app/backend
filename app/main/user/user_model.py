""" Module for defining User related models. """

import marshmallow as ma
import pytz

from app.main import db, bcrypt


class User(db.Model):
    """User Entity Model, defining user database table"""

    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(50), nullable=False, unique=True)
    email = db.Column(db.String(255), nullable=False, unique=True)
    password_hash = db.Column(db.String(100), nullable=False)
    language = db.Column(db.String(100), nullable=False)
    time_zone = db.Column(db.String(100), nullable=False)
    registered_on = db.Column(db.DateTime, nullable=False)
    last_update = db.Column(db.DateTime, nullable=False)
    admin = db.Column(db.Boolean, nullable=False, default=False)
    premium = db.Column(db.Boolean, nullable=False, default=False)

    @property
    def password(self):
        raise AttributeError('password: write-only field')

    @password.setter
    def password(self, password):
        self.password_hash = bcrypt.generate_password_hash(password).decode('utf-8')

    def check_password(self, password):
        return bcrypt.check_password_hash(self.password_hash, password)

    def __repr__(self):
        return "<User id: '{}', name: '{}'>".format(self.id, self.username)


class UserSchema(ma.Schema):
    """User Schema Model for serialization and deserialization"""

    class Meta:
        ordered = True

    id = ma.fields.Integer(dump_only=True)
    username = ma.fields.String(
        required=True,
        validate=ma.validate.Regexp(
            '[a-zA-Z0-9]{3,}',
            error='invalid username: only letters and 0-9 are allowed'
        ),
    )
    email = ma.fields.String(
        required=True,
        validate=ma.validate.Regexp(
            '^[a-z0-9_.+-]+@[a-z0-9-]+\.[a-z0-9-.]+$',
            error='invalid email'
        ),
    )
    password = ma.fields.String(
        required=True,
        validate=ma.validate.Regexp(
            '[a-zA-Z0-9!§$%&#]{8,}',
            error='invalid password: only a-z,A-Z,0-9,!§$%&# are valid (min.length 8)'
        ),
        load_only=True,
    )
    language = ma.fields.String(
        validate=ma.validate.OneOf(
            choices=['ENG', 'DE'],
            error='invalid language'
        ),
    )
    time_zone = ma.fields.String(
        validate=ma.validate.OneOf(
            choices=pytz.common_timezones,
            error='invalid timezone'
        ),
    )
    registered_on = ma.fields.DateTime(dump_only=True)
    last_update = ma.fields.DateTime(dump_only=True)
    admin = ma.fields.Boolean(dump_only=True)
    premium = ma.fields.Boolean(dump_only=True)


class UserPartialSchema(UserSchema):
    """User Update Schema Model for serialization and deserialization"""
    pass


class UserAccessSchema(ma.Schema):
    """User Schema for access-level updates"""

    class Meta:
        ordered = True

    id = ma.fields.Integer(dump_only=True)
    username = ma.fields.String(dump_only=True)
    registered_on = ma.fields.DateTime(dump_only=True)
    last_update = ma.fields.DateTime(dump_only=True)
    admin = ma.fields.Boolean()
    premium = ma.fields.Boolean()


class UserQuerySchema(ma.Schema):
    """User Schema for query parameters"""

    class Meta:
        ordered = True

    ids = ma.fields.List(ma.fields.Integer())
