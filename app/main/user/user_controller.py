""" Module for Routing of User related requests. """

from flask.views import MethodView
from flask_jwt_extended import fresh_jwt_required
from flask_smorest import Blueprint

from app.main.user.user_model import UserSchema, UserPartialSchema, UserAccessSchema, UserQuerySchema
from app.main.user.user_service import UserService
from app.main.utils.controller_utils import ErrorDocs, AccessHandler

user_bp = Blueprint('users', 'users', url_prefix='/', description='Users related operations')


@user_bp.route('users')
class UsersAdmin(MethodView):
    """
    Methods:
        - GET: get all users
    """

    @fresh_jwt_required
    @AccessHandler.required_access_level('Admin')
    @user_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401]))
    @user_bp.arguments(UserQuerySchema, location='query', as_kwargs=True)
    @user_bp.response(UserSchema(many=True), code=200, description='Successfully retrieved users.')
    def get(self, **kwargs):
        """List all registered users."""
        return UserService.read_all(**kwargs)


@user_bp.route('users')
class User(MethodView):
    """
    Methods:
        - POST: create a user
    """

    @user_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 409, 422]))
    @user_bp.arguments(UserSchema, location='json')
    @user_bp.response(UserSchema, code=201, description='Successfully created user.')
    def post(self, user_data):
        """Create a new user."""
        return UserService.create(user_data=user_data)


@user_bp.route('users/<int:id>/', parameters=[
    {'name': 'id', 'in': 'path', 'description': 'user id'},
])
class UserById(MethodView):
    """
    Methods:
        - GET: get a user
        - PUT: update a user
        - DELETE: delete a user
    """

    @fresh_jwt_required
    @AccessHandler.required_access_level('User')
    @user_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401, 404]))
    @user_bp.response(UserSchema, code=200, description='Successfully retrieved user.')
    def get(self, id):
        """Get a user given its id."""
        return UserService.read(id=id)

    @fresh_jwt_required
    @AccessHandler.required_access_level('User')
    @user_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401, 404, 422]))
    @user_bp.arguments(UserPartialSchema(partial=True), location='json')
    @user_bp.response(UserSchema, code=200, description='Successfully updated user.')
    def put(self, user_data, id):
        """Update a user given its id."""
        return UserService.update(id=id, user_data=user_data)

    @fresh_jwt_required
    @AccessHandler.required_access_level('User')
    @user_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401, 404]))
    @user_bp.response(UserSchema, code=200, description='Successfully deleted user.')
    def delete(self, id):
        """Delete a user given its id."""
        return UserService.delete(id=id)


@user_bp.route('users/<int:id>/access-level', parameters=[
    {'name': 'id', 'in': 'path', 'description': 'user id'},
])
class UserAccessLevels(MethodView):
    """
    Methods:
        - PUT: update a users access level
    """

    @fresh_jwt_required
    @AccessHandler.required_access_level('Admin')
    @user_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401, 404, 422]))
    @user_bp.arguments(UserAccessSchema, location='json')
    @user_bp.response(UserSchema, code=200, description='Successfully updated user.')
    def put(self, user_data, id):
        """Update the access level of a user."""
        return UserService.update(id=id, user_data=user_data)
