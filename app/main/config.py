"""
Module to define different configurations for deploying the application
Development, Testing and Production
"""

import os
from datetime import timedelta

######################################### CONFIGS #########################################

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    # APP
    APP_NAME = 'GaIn - Gartz Investment API'
    # API DOC
    OPENAPI_VERSION = '3.0.2'
    OPENAPI_SWAGGER_UI_URL = 'https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.24.2/'
    OPENAPI_URL_PREFIX = '/gartz-invest'
    OPENAPI_REDOC_PATH = '/redoc'
    OPENAPI_SWAGGER_UI_PATH = '/swagger'
    API_VERSION = '1.0'
    # AUTHENTICATION
    JWT_SECRET_KEY = os.getenv('JWT_SECRET_KEY')
    JWT_HEADER_TYPE = ''
    JWT_ACCESS_TOKEN_EXPIRES = timedelta(minutes=30)


class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://{}:{}@{}:{}/gartz_invest_dev'.format(
        os.getenv('POSTGRES_USER_NAME'), os.getenv('POSTGRES_PASSWORD'),
        os.getenv('POSTGRES_HOST_NAME'), os.getenv('POSTGRES_PORT'))
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class TestingConfig(Config):
    DEBUG = True
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://{}:{}@{}:{}/gartz_invest_test'.format(
        os.getenv('POSTGRES_USER_NAME'), os.getenv('POSTGRES_PASSWORD'),
        os.getenv('POSTGRES_HOST_NAME'), os.getenv('POSTGRES_PORT'))
    PRESERVE_CONTEXT_ON_EXCEPTION = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class ProductionConfig(Config):
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://{}:{}@{}:{}/gartz_invest_prod'.format(
        os.getenv('POSTGRES_USER_NAME'), os.getenv('POSTGRES_PASSWORD'),
        os.getenv('POSTGRES_HOST_NAME'), os.getenv('POSTGRES_PORT'))
    SQLALCHEMY_TRACK_MODIFICATIONS = False


config_by_name = {'dev': DevelopmentConfig, 'test': TestingConfig, 'prod': ProductionConfig}
