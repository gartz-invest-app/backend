""" Module for defining Service for Account controller. """

from datetime import datetime

from flask_smorest import abort

from app.main import db
from app.main.account.account_model import Account
from app.main.utils.service_utils import GenericService


class AccountService(GenericService):

    @classmethod
    def read_all(cls, ids=None, user_ids=None):
        """
        Read accounts by filter.

        Parameters
        ----------
        ids : list of int, optional
            ids of contracts to filter contracts by
        user_ids : list of int, optional
            ids of users to filter contracts by

        Returns
        -------
        list of :class:`Account`
            List of accounts matching the filter.
        """

        if ids is None:
            ids_filter = True
        else:
            ids_filter = Account.id.in_(ids)

        if user_ids is None:
            user_ids_filter = True
        else:
            user_ids_filter = Account.user_id.in_(user_ids)

        return Account.query.filter(ids_filter, user_ids_filter).all()

    @classmethod
    def read_all_by_user(cls, user_id, **kwargs):
        """
        Read accounts of a user by filter.

        Parameters
        ----------
        user_id : int
            id of user for which to retrieve accounts
        **kwargs : optional
            keyword arguments for filtering

        Returns
        -------
        list of :class:`Account`
        `   List of accounts of the user matching the filter.
        """
        super().validate_user_permission(user_id)

        return cls.read_all(user_ids=[user_id], **kwargs)

    @classmethod
    def create(cls, account_data, user_id):
        """
        Create a new account for a user.

        Parameters
        ----------
        account_data : dict
            deserialized request data for account (immutable)
        user_id : int
            id of user for which to create new account

        Returns
        -------
        :class:`Account`
            Created account.
        """
        super().validate_user_permission(user_id)

        account = Account.query.filter((Account.user_id == user_id) & (Account.name == account_data['name'])).first()

        if not account:
            new_account = Account(**account_data)
            new_account.user_id = user_id
            new_account.setup_date = new_account.last_update = datetime.utcnow()

            db.session.add(new_account)
            db.session.commit()

            return new_account
        else:
            abort(409, msg='Account already exists.')

    @classmethod
    def update(cls, account_data, user_id, id):
        """
        Update an account of a user by id.

        Parameters
        ----------
        account_data : dict
            deserialized request data for account (immutable)
        user_id : int
            id of user for which to update account
        id : int
            id of account to update

        Returns
        -------
        :class:`Account`
            Updated account.
        """
        super().validate_user_permission(user_id)

        account = Account.query.filter((Account.id == id) & (Account.user_id == user_id)).first()

        if not account:
            abort(404, message='There is no Account with the given identifier for that user.')

        for k, v in account_data.items():
            setattr(account, k, v)
        account.last_update = datetime.utcnow()

        db.session.add(account)
        db.session.commit()

        return account

    @classmethod
    def read(cls, user_id, id):
        """
        Read an account of a user by id.

        Parameters
        ----------
        id : int
            id of account to get from database
        user_id : int
            id of user for which to read account

        Returns
        -------
        :class:`Account`
            Retrieved account.
        """
        super().validate_user_permission(user_id)

        account = Account.query.filter((Account.id == id) & (Account.user_id == user_id)).first()

        if not account:
            abort(404, message='There is no Account with the given identifier for that user.')

        return account

    @classmethod
    def delete(cls, user_id, id):
        """
        Delete an account of a user by id.

        Parameters
        ----------
        id : int
            id of account to delete from database
        user_id : int
            id of user for which to delete account

        Returns
        -------
        :class: `Account`
            Deleted account.
        """
        super().validate_user_permission(user_id)

        account = Account.query.filter((Account.id == id) & (Account.user_id == user_id)).first()

        if not account:
            abort(404, message='There is no Account with the given identifier for that user.')

        db.session.delete(account)
        db.session.commit()

        return account
