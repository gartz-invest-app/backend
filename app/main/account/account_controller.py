""" Module for Routing of Account related requests. """

from flask.views import MethodView
from flask_jwt_extended import fresh_jwt_required
from flask_smorest import Blueprint

from app.main.account.account_model import AccountSchema, AccountPartialSchema, AccountQuerySchema
from app.main.account.account_service import AccountService
from app.main.utils.controller_utils import ErrorDocs, AccessHandler

acc_bp = Blueprint('users/accounts', 'users/accounts', url_prefix='/users', description='Accounts related operations')


@acc_bp.route('/accounts')
class AccountsAdmin(MethodView):
    """
    Methods:
        - GET: get all accounts
    """

    @fresh_jwt_required
    @AccessHandler.required_access_level('Admin')
    @acc_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401]))
    @acc_bp.arguments(AccountQuerySchema, location='query', as_kwargs=True)
    @acc_bp.response(AccountSchema(many=True), code=200, description='Successfully retrieved accounts.')
    def get(self, **kwargs):
        """List all accounts."""
        return AccountService.read_all(**kwargs)


@acc_bp.route('/<int:user_id>/accounts', parameters=[
    {'name': 'user_id', 'in': 'path', 'description': 'user id'},
])
class AccountsUser(MethodView):
    """
    Methods:
        - GET: get all accounts of a user
    """

    @fresh_jwt_required
    @AccessHandler.required_access_level('User')
    @acc_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401, 404]))
    @acc_bp.arguments(AccountQuerySchema(exclude=['user_ids']), location='query', as_kwargs=True)
    @acc_bp.response(AccountSchema(many=True), code=200, description='Successfully retrieved accounts.')
    def get(self, user_id, **kwargs):
        """List all accounts of a user."""
        return AccountService.read_all_by_user(user_id=user_id, **kwargs)


@acc_bp.route('/<int:user_id>/accounts', parameters=[
    {'name': 'user_id', 'in': 'path', 'description': 'user id'},
])
class Account(MethodView):
    """
    Methods:
        - POST: create an account for a user
    """

    @fresh_jwt_required
    @AccessHandler.required_access_level('User')
    @acc_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401, 409, 422]))
    @acc_bp.arguments(AccountSchema, location='json')
    @acc_bp.response(AccountSchema, code=201, description='Successfully created account.')
    def post(self, account_data, user_id):
        """Create a new account for a user."""
        return AccountService.create(account_data=account_data, user_id=user_id)


@acc_bp.route('/<int:user_id>/accounts/<int:id>', parameters=[
    {'name': 'user_id', 'in': 'path', 'description': 'user id'},
    {'name': 'id', 'in': 'path', 'description': 'account id'},
])
class AccountById(MethodView):
    """
    Methods:
        - GET: get an account of a user
        - PUT: update an account of a user
        - DELETE: delete an account of a user
    """

    @fresh_jwt_required
    @AccessHandler.required_access_level('User')
    @acc_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401, 404]))
    @acc_bp.response(AccountSchema, code=200, description='Successfully retrieved account.')
    def get(self, user_id, id):
        """Get an account of a user given its id."""
        return AccountService.read(user_id=user_id, id=id)

    @fresh_jwt_required
    @AccessHandler.required_access_level('User')
    @acc_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401, 404, 422]))
    @acc_bp.arguments(AccountPartialSchema(partial=True), location='json')
    @acc_bp.response(AccountSchema, code=200, description='Successfully updated account.')
    def put(self, account_data, user_id, id):
        """Update an account of a user given its id."""
        return AccountService.update(account_data=account_data, user_id=user_id, id=id)

    @fresh_jwt_required
    @AccessHandler.required_access_level('User')
    @acc_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401, 404]))
    @acc_bp.response(AccountSchema, code=200, description='Successfully deleted account.')
    def delete(self, user_id, id):
        """Delete an account of a user given its id."""
        return AccountService.delete(user_id=user_id, id=id)
