""" Module for defining Account related models. """

import marshmallow as ma
from sqlalchemy import UniqueConstraint

from app.main import db


class Account(db.Model):
    """Account Entity Model, defining account database table"""

    __tablename__ = 'account'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255), nullable=False)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='CASCADE'), nullable=False)
    user = db.relationship(
        'User',
        backref=db.backref('accounts', lazy=True)
    )

    currency = db.Column(db.String(10), nullable=False)
    setup_balance = db.Column(db.Float(precision=2), nullable=False)
    setup_date = db.Column(db.DateTime, nullable=False)
    last_update = db.Column(db.DateTime, nullable=False)
    last_transaction = db.Column(db.Date, nullable=True)
    note = db.Column(db.Text, nullable=False)
    active = db.Column(db.Boolean, nullable=False, default=True)

    __table_args__ = (
        UniqueConstraint('user_id', 'name', name='user_id_account_name_uk'),
    )

    def __repr__(self):
        return "<Account '{}' of User '{}'>".format(self.name, self.user.username)


class AccountSchema(ma.Schema):
    """Account Schema Model for serialization and validation of request data"""

    class Meta:
        ordered = True

    id = ma.fields.Integer(dump_only=True)
    name = ma.fields.String(
        required=True,
        validate=ma.validate.Regexp(
            '[a-zA-Z0-9_.&%$-]{3,}',
            error='invalid account name (only letters, 0-9 and _ . & % $ - are allowed, min. 3 symbols)'
        )
    )
    currency = ma.fields.String(
        required=True,
        validate=ma.validate.OneOf(
            choices=['EUR', 'USD'],
            error='invalid currency'
        ),
    )
    user_id = ma.fields.Integer(dump_only=True)
    setup_balance = ma.fields.Float(required=True)
    setup_date = ma.fields.DateTime(dump_only=True)
    last_update = ma.fields.DateTime(dump_only=True)
    last_transaction = ma.fields.Date(dump_only=True)
    note = ma.fields.String(missing='')
    active = ma.fields.Boolean(missing=True)


class AccountPartialSchema(AccountSchema):
    """Account Update Schema Model for serialization and validation of request data"""
    pass


class AccountQuerySchema(ma.Schema):
    """Account Schema for query parameters"""

    class Meta:
        ordered = True

    ids = ma.fields.List(ma.fields.Integer())
    user_ids = ma.fields.List(ma.fields.Integer())
