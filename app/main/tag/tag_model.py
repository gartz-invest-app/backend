"""
Module for defining Tag models.
A Tag is used to classify transactions or contracts.
"""

import marshmallow as ma
from sqlalchemy import UniqueConstraint

from app.main import db
from app.main.utils.model_utils import (tag_group_association_table,
                                        contract_tag_association_table,
                                        transaction_tag_association_table)


class Tag(db.Model):
    """Tag Entity Model, defining tag database table"""

    __tablename__ = 'tag'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255), nullable=False)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='CASCADE'), nullable=False)
    user = db.relationship(
        'User',
        backref=db.backref('tags', lazy=True)
    )

    transactions = db.relationship('Transaction', secondary=transaction_tag_association_table, back_populates='tags')
    contracts = db.relationship('Contract', secondary=contract_tag_association_table, back_populates='tags')
    tag_groups = db.relationship('TagGroup', secondary=tag_group_association_table, back_populates='tags')

    note = db.Column(db.Text, nullable=False)

    __table_args__ = (
        UniqueConstraint('user_id', 'name', name='user_id_tag_name_uk'),
    )

    def __repr__(self):
        return "<Tag id: '{}', name: '{}', user: '{}' >".format(
            self.id, self.name, self.user.username)


class TagSchema(ma.Schema):
    """Tag Schema Model for serialization and deserialization"""

    class Meta:
        ordered = True

    id = ma.fields.Integer(dump_only=True)
    name = ma.fields.String(
        required=True,
        validate=ma.validate.Regexp(
            '[a-zA-Z0-9_.&%$-]{3,}',
            error='invalid tag name (only letters, 0-9 and _ . & % $ - are allowed, min. 3 symbols)'
        ),
    )
    user_id = ma.fields.Integer(dump_only=True)
    note = ma.fields.String(missing='')


class TagPartialSchema(TagSchema):
    """Tag Update Schema Model for serialization and deserialization"""
    pass


class TagQueryParams(ma.Schema):
    """Tag Schema for query parameters"""

    class Meta:
        ordered = True

    ids = ma.fields.List(ma.fields.Integer())
    user_ids = ma.fields.List(ma.fields.Integer())
    tag_group_ids = ma.fields.List(ma.fields.Integer())
