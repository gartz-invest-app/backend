""" Module for Routing of Tag related requests. """

from flask.views import MethodView
from flask_jwt_extended import fresh_jwt_required
from flask_smorest import Blueprint

from app.main.tag.tag_model import TagSchema, TagPartialSchema, TagQueryParams
from app.main.tag.tag_service import TagService
from app.main.utils.controller_utils import ErrorDocs, AccessHandler

tag_bp = Blueprint('users/tags', 'users/tags', url_prefix='/users', description='Tags related operations')


@tag_bp.route('/tags')
class TagsAdmin(MethodView):
    """
    Methods:
        - GET: get tags of all users
    """

    @fresh_jwt_required
    @AccessHandler.required_access_level('Admin')
    @tag_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401]))
    @tag_bp.arguments(TagQueryParams, location='query', as_kwargs=True)
    @tag_bp.response(TagSchema(many=True), code=200, description='Successfully retrieved tags.')
    def get(self, **kwargs):
        """List all tags."""
        return TagService.read_all(**kwargs)


@tag_bp.route('/<int:user_id>/tags', parameters=[
    {'name': 'user_id', 'in': 'path', 'description': 'user id'},
])
class TagsUser(MethodView):
    """
    Methods:
        - GET: get tags of a user
    """

    @fresh_jwt_required
    @AccessHandler.required_access_level('User')
    @tag_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401]))
    @tag_bp.arguments(TagQueryParams(exclude=['user_ids']), location='query', as_kwargs=True)
    @tag_bp.response(TagSchema(many=True), code=200, description='Successfully retrieved tags.')
    def get(self, user_id, **kwargs):
        """List all tags for a user."""
        return TagService.read_all_by_user(user_id=user_id, **kwargs)


@tag_bp.route('/<int:user_id>/tags', parameters=[
    {'name': 'user_id', 'in': 'path', 'description': 'user id'},
])
class Tag(MethodView):
    """
    Methods:
        - POST: create a new tag for a user
    """

    @fresh_jwt_required
    @AccessHandler.required_access_level('User')
    @tag_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401, 404, 409, 422]))
    @tag_bp.arguments(TagSchema, location='json')
    @tag_bp.response(TagSchema, code=201, description='Successfully created tag.')
    def post(self, tag_data, user_id):
        """Create a new tag for a user."""
        return TagService.create(tag_data=tag_data, user_id=user_id)


@tag_bp.route('/<int:user_id>/tags/<int:id>', parameters=[
    {'name': 'user_id', 'in': 'path', 'description': 'user id'},
    {'name': 'id', 'in': 'path', 'description': 'tag id'},
])
class TagById(MethodView):
    """
    Methods:
        - GET: get an tag of a user
        - PUT: update an tag of a user
        - DELETE: delete an tag of a user
    """

    @fresh_jwt_required
    @AccessHandler.required_access_level('User')
    @tag_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401, 404]))
    @tag_bp.response(TagSchema, code=200, description='Successfully retrieved tag.')
    def get(self, user_id, id):
        """Get a tag of a user given its id."""
        return TagService.read(user_id=user_id, id=id)

    @fresh_jwt_required
    @AccessHandler.required_access_level('User')
    @tag_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401, 404, 422]))
    @tag_bp.arguments(TagPartialSchema(partial=True), location='json')
    @tag_bp.response(TagSchema, code=200, description='Successfully updated tag.')
    def put(self, tag_data, user_id, id):
        """Update a tag of a user given its id."""
        return TagService.update(tag_data=tag_data, user_id=user_id, id=id)

    @fresh_jwt_required
    @AccessHandler.required_access_level('User')
    @tag_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[400, 401, 404]))
    @tag_bp.response(TagSchema, code=200, description='Successfully deleted tag.')
    def delete(self, user_id, id):
        """Delete a tag of a user given its id."""
        return TagService.delete(user_id=user_id, id=id)
