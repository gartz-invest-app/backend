from flask_smorest import abort

from app.main import db
from app.main.tag.tag_model import Tag
from app.main.tag_group.tag_group_model import TagGroup
from app.main.utils.model_utils import tag_group_association_table
from app.main.utils.service_utils import GenericService


class TagService(GenericService):

    @classmethod
    def read_all(cls, ids=None, user_ids=None, tag_group_ids=None):
        """
        Read tags by filter.

        Parameters
        ----------
        ids : list of int
            ids of tags to filter by
        user_ids : list of int
            user ids to filter tags by
        tag_group_ids : list of int
            tag group ids to filter tags by

        Returns
        -------
        list of :class:`Tag`
            List of all tags matching the filter.
        """
        if ids is None:
            ids_filter = True
        else:
            ids_filter = Tag.id.in_(ids)

        if user_ids is None:
            user_ids_filter = True
        else:
            user_ids_filter = Tag.user_id.in_(user_ids)

        if tag_group_ids is None:
            tag_group_ids_filter = True
        else:
            tag_group_ids_filter = tag_group_association_table.c.tag_group_id.in_(tag_group_ids)

        return Tag.query.join(tag_group_association_table, isouter=True).join(TagGroup, isouter=True) \
            .filter(ids_filter, user_ids_filter, tag_group_ids_filter).all()

    @classmethod
    def read_all_by_user(cls, user_id, **kwargs):
        """
        Read all tags of a user by filter.

        Parameters
        ----------
        user_id : int
            id of user to get tags for
        **kwargs : optional
            keyword arguments for filtering

        Returns
        -------
        list of :class:`Tag`
            List of tags of the user matching the filter.
        """
        cls.validate_user_permission(user_id)

        return cls.read_all(user_ids=[user_id], **kwargs)

    @classmethod
    def create(cls, tag_data, user_id):
        """
        Create a new tag for a user.

        Parameters
        ----------
        tag_data : dict
            deserialized request data for tag (immutable)
        user_id : int
            id of user to create tag for

        Returns
        -------
        :class:`Tag`
            Created tag.
        """
        cls.validate_user_permission(user_id)

        tag = Tag.query.filter(Tag.user_id == user_id,
                               Tag.name == tag_data['name']).first()

        if not tag:
            new_tag = Tag(**tag_data)
            new_tag.user_id = user_id

            db.session.add(new_tag)
            db.session.commit()

            return new_tag
        else:
            abort(409, msg='Tag already exists.')

    @classmethod
    def update(cls, tag_data, user_id, id):
        """
        Update a tag of a user by id.

        Parameters
        ----------
        tag_data : dict
            deserialized request data for tag (immutable)
        user_id : int
            id of user to update tag for
        id : int
            id of tag to update

        Returns
        -------
        :class:`Tag`
            Updated tag.
        """
        cls.validate_user_permission(user_id)

        tag = Tag.query.filter(Tag.id == id,
                               Tag.user_id == user_id).first()

        if not tag:
            abort(404, message='There is no Tag with the given identifier for this user.')

        for k, v in tag_data.items():
            setattr(tag, k, v)

        db.session.add(tag)
        db.session.commit()

        return tag

    @classmethod
    def read(cls, user_id, id):
        """
        Read a tag of a user by id.

        Parameters
        ----------
        user_id : int
            id of user to get tag for
        id : int
            id of tag to get from database

        Returns
        -------
        :class:`Tag`
            Retrieved tag.
        """
        cls.validate_user_permission(user_id)

        tag = Tag.query.filter(Tag.id == id,
                               Tag.user_id == user_id).first()

        if not tag:
            abort(404, message='There is no Tag with the given identifier for this user.')

        return tag

    @classmethod
    def delete(cls, user_id, id):
        """
        Delete a tag by id.

        Parameters
        ----------
        user_id : int
            id of user to delete tag for
        id : int
            id of tag to delete from database

        Returns
        -------
        :class:`Tag`
            Deleted tag.
        """
        cls.validate_user_permission(user_id)

        tag = Tag.query.filter(Tag.id == id,
                               Tag.user_id == user_id).first()

        if not tag:
            abort(404, message='There is no Tag with the given identifier for this user.')

        db.session.delete(tag)
        db.session.commit()

        return tag
