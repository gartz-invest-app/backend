import os

import pytz
from apscheduler.executors.pool import ThreadPoolExecutor, ProcessPoolExecutor
from apscheduler.jobstores.sqlalchemy import SQLAlchemyJobStore
from apscheduler.schedulers.background import BackgroundScheduler

from app.main.config import config_by_name
from app.main.scheduler.tasks import InstantiateContractTransactions


class FlaskAPScheduler:
    """Custom wrapper class for managing APScheduler in Flask"""
    jobstores = {'default': SQLAlchemyJobStore(
        url=config_by_name[os.getenv('GARTZ_INVEST_ENV') or 'dev'].SQLALCHEMY_DATABASE_URI)}
    executors = {'default': ThreadPoolExecutor(20), 'processpool': ProcessPoolExecutor(5)}
    job_defaults = {'coalesce': True, 'replace_existing': True, 'max_instances': 1}

    def __init__(self, app=None):
        self.app = app
        self.scheduler = BackgroundScheduler(jobstore=self.jobstores, executors=self.executors,
                                             job_defaults=self.job_defaults, timezone=pytz.utc)

    def init_app(self, app):
        if not app.debug or os.environ.get("WERKZEUG_RUN_MAIN") == "true":
            self.app = app
            self.scheduler.start()
            self.add_jobs()

    def add_context_job(self, func, job_kwargs):
        def context_func(fn):
            def func_wrapper():
                with self.app.app_context():
                    return fn()

            return func_wrapper

        self.scheduler.add_job(context_func(func), **job_kwargs)

    def add_jobs(self):
        self.add_context_job(InstantiateContractTransactions.func,
                             job_kwargs={
                                 'id': InstantiateContractTransactions.__name__,
                                 **InstantiateContractTransactions.trigger_args
                             })
