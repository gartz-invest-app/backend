from datetime import datetime

from dateutil.relativedelta import relativedelta


class InstantiateContractTransactions:
    """Class defining a scheduler job for contract transaction instantiation."""

    trigger_args = {
        'trigger': 'cron',
        'hour': '*'}

    @staticmethod
    def func():
        from app.main import db
        from app.main.contract.contract_model import Contract

        print('executing job: {}'.format(InstantiateContractTransactions.__name__))
        utc_now_with_padding = datetime.utcnow() + relativedelta(minutes=30)
        contracts = Contract.query.filter(Contract.next_payment_date <= utc_now_with_padding).all()

        transactions = [contract.instantiate_transaction() for contract in contracts]

        for transaction in transactions:
            if (not transaction.account.last_transaction) or (transaction.date > transaction.account.last_transaction):
                transaction.account.last_transaction = transaction.date

        for contract in contracts:
            next_payment_date = contract.next_payment_date + relativedelta(months=contract.payment_method)
            if next_payment_date.date() <= contract.end_date:
                contract.next_payment_date = next_payment_date
            else:
                contract.next_payment_date = None

        db.session.add_all(transactions)
        db.session.add_all(contracts)
        db.session.commit()
