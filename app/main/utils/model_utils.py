"""Module defining association tables for many-to-many relations between models."""

from app.main import db

# defining association table for many to many relationship between tags and tag-groups
tag_group_association_table = db.Table('tag_group_association', db.Model.metadata,
                                       db.Column('tag_group_id', db.Integer, db.ForeignKey('tag_group.id')),
                                       db.Column('tag_id', db.Integer, db.ForeignKey('tag.id')))

# defining association table for many to many relationship between transactions and tags
transaction_tag_association_table = db.Table('transaction_tag_association', db.Model.metadata,
                                             db.Column('transaction_id', db.Integer, db.ForeignKey('transaction.id')),
                                             db.Column('tag_id', db.Integer, db.ForeignKey('tag.id')))

# defining association table for many to many relationship between contracts and tags
contract_tag_association_table = db.Table('contract_tag_association', db.Model.metadata,
                                          db.Column('contract_id', db.Integer, db.ForeignKey('contract.id')),
                                          db.Column('tag_id', db.Integer, db.ForeignKey('tag.id')))
