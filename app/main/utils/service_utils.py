from flask_jwt_extended import get_jwt_identity
from flask_smorest import abort

from app.main.user.user_model import User


class GenericService:

    @staticmethod
    def validate_user_permission(user_id):
        """
        Validate user permission for input parameter.

        Parameters
        ----------
        user_id : int
            user id to check permission for

        Returns
        -------
        tuple
            current_user : :class:`User`
                currently authenticated user
            requested_user : :class:`User`
                user operated on
        """

        current_user = User.query.filter(User.id == get_jwt_identity()).first()
        requested_user = User.query.filter(User.id == user_id).first()

        if not requested_user:
            abort(404, message='There is no User with for the given identifier.')

        if (not current_user.admin) and (requested_user.id != current_user.id):
            abort(401, message='You do not have the required access rights.')

        return current_user, requested_user
