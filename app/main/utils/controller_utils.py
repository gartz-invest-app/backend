""" Module for defining controller related utilities. """

from functools import wraps

from flask import jsonify
from flask_jwt_extended import get_jwt_identity
from flask_smorest import abort

from app.main import jwt
from app.main.user.user_model import User


class JWTHandler:
    """Custom ErrorHandler for JWT authentication."""

    @staticmethod
    @jwt.unauthorized_loader
    def default_unauthorized_handler(expired_token):
        return jsonify({'code': 401,
                        'status': 'Unauthorized',
                        'message': 'Missing authentication.',
                        'expired token': expired_token}), 401


class ErrorDocs:
    """Default error documentation"""

    ERROR_DOCS = {
        400: {'description': 'Bad Request.'},
        401: {'description': 'Unauthorized.'},
        404: {'description': 'Resource not found.'},
        409: {'description': 'Entity already exists.'},
        422: {'description': 'Unprocessable Entity. Input data is invalid.'}
    }

    @classmethod
    def get_error_docs(cls, error_codes):
        """
        Get error documentation given a list of error codes.

        Parameters
        ----------
        error_codes : list of int
            List of error codes to document

        Returns
        -------
        error_docs : dict
            Dict of error documentation
        """
        error_docs = {}
        for code in error_codes:
            error_docs[code] = cls.ERROR_DOCS[code]
        return error_docs


class AccessHandler:
    """Handler for regulating endpoint access permissions"""

    ACCESS_LEVELS = {
        'User': 1,
        'Premium': 2,
        'Admin': 3}

    DEFAULT_ACCESS_LEVEL = ACCESS_LEVELS['User']

    @classmethod
    def required_access_level(cls, req_access_lvl):
        """
        Decorator for defining required access level for the decorated function.

        There are three access levels: 'User' (1), 'Premium' (2), 'Admin' (3).
        The user calling the function has to have at least the required access level.

        If the user does not have the required access rights, the transaction is aborted with 401 (Unauthorized).
        """

        def decorator(fn):
            @wraps(fn)
            def wrapper(*args, **kwargs):
                current_user = User.query.filter(User.id == get_jwt_identity()).first()

                if current_user.admin:
                    user_access_lvl = cls.ACCESS_LEVELS['Admin']
                elif current_user.premium:
                    user_access_lvl = cls.ACCESS_LEVELS['Premium']
                else:
                    user_access_lvl = cls.DEFAULT_ACCESS_LEVEL

                if user_access_lvl >= cls.ACCESS_LEVELS[req_access_lvl]:
                    return fn(*args, **kwargs)

                else:
                    abort(401,
                          message='You do not have the required access level ({} required).'.format(req_access_lvl))

            return wrapper

        return decorator
