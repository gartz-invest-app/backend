"""Module for defining authentication related models."""

import marshmallow as ma


class AuthSchema(ma.Schema):
    """Authentication Schema Model for serialization and validation of request data"""

    class Meta:
        ordered = True

    identifier = ma.fields.String(
        required=True,
        validate=ma.validate.Length(min=3,
                                    error='invalid identifier (min. length 3)'
                                    ),
    )
    password = ma.fields.String(
        required=True,
        validate=ma.validate.Regexp(
            '[a-zA-Z0-9!§$%&#]{8,}',
            error='invalid password: only a-z,A-Z,0-9,!§$%&# are valid (min.length 8)'
        ),
        load_only=True,
    )
