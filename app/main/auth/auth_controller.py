""" Module for Routing of Authentication requests. """

from flask.views import MethodView
from flask_smorest import Blueprint

from app.main.auth.auth_model import AuthSchema
from app.main.auth.auth_service import AuthService
from app.main.utils.controller_utils import ErrorDocs

auth_bp = Blueprint('authentication', 'authentication',
                    url_prefix='/authentication',
                    description='Authentication related operations')


@auth_bp.route('/login')
class Login(MethodView):
    """
    Methods:
        - POST: Login
    """

    @auth_bp.arguments(AuthSchema, location='json')
    @auth_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[401, 404, 422]))
    @auth_bp.response(code=200, description='Successful login.')
    def post(self, auth_data):
        """Login user"""
        return AuthService.login(auth_data=auth_data)


@auth_bp.route('/login-form')
class Login(MethodView):
    """
    Methods:
        - POST: Login
    """

    @auth_bp.arguments(AuthSchema, location='form')
    @auth_bp.doc(responses=ErrorDocs.get_error_docs(error_codes=[401, 404, 422]))
    @auth_bp.response(code=200, description='Successful login.')
    def post(self, auth_data):
        """Login user"""
        return AuthService.login(auth_data=auth_data)
