from flask_jwt_extended import create_access_token
from flask_smorest import abort

from app.main.user.user_model import User, UserSchema


class AuthService:

    @classmethod
    def login(cls, auth_data):
        """
        Login user given email/username and password.

        Parameters
        ----------
        auth_data : dict
            deserialized request data for authentication (immutable)

        Returns
        -------
        dict
            access_token : str
                JWT access token for user
            user : :class:`User`
                data of logged in user
        """
        user = User.query.filter(
            (User.username == auth_data['identifier']) | (User.email == auth_data['identifier'])).first()

        if not user:
            abort(404, message='There is no {} with identifier: {}'.format(User.__name__, auth_data['identifier']))

        if user.check_password(auth_data['password']):
            access_token = create_access_token(identity=user.id, fresh=True)
            return {'access_token': access_token, 'user': UserSchema().dump(user)}
        else:
            return abort(409, message='Invalid Password!')
