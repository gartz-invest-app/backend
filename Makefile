.PHONY: clean run-tests run-app db-upgrade

clean:
	find . -name "__pycache__" -type d -exec rm -r "{}" \;
	find . -name ".pytest_cache" -type d -exec rm -r "{}" \;
	find . -type f -name '*.log' -delete

run-tests:
	python manage.py test

run-app:
	python manage.py run

db-upgrade:
	python manage.py db upgrade