#!/bin/bash

psql << EOF
CREATE USER gartz WITH PASSWORD '$POSTGRES_PASSWORD';

CREATE DATABASE gartz_invest_dev;
CREATE DATABASE gartz_invest_test;

GRANT ALL PRIVILEGES ON DATABASE gartz_invest_dev TO gartz;
GRANT ALL PRIVILEGES ON DATABASE gartz_invest_test TO gartz;
EOF

