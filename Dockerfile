FROM python:3.7
WORKDIR /app
COPY requirements.txt /app
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
COPY . /app
EXPOSE 5000
CMD ["/bin/bash", "/app/startup-script.sh"]